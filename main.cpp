#include <iostream>
#include <fstream>

using namespace std;

class Node{
    public:
    Node* right;
    Node* left;
    double data;
};

Node* add(Node* root, double data){
    if(root == NULL){
        Node *node = new Node();
        node->right = NULL;
        node->left = NULL;
        node->data = data;

        return node;
    }

    if(data < root->data) root->left = add(root->left, data);

    else if(data > root->data) root->right = add(root->right, data);

    else return root;
}

Node* rem(Node* root, double data){
    if(root == NULL) return root;

    else if(data < root->data) root->left = rem(root->left, data);

    else if(data > root->data) root->right = rem(root->right, data);

    if(root->data == data){
        if(root->left == NULL && root->right == NULL){
            return root = NULL;
        }
        else if(root->left == NULL && root->right != NULL){
            *root = *root->right;
        }
        else if(root->right == NULL && root->left != NULL){\
            *root = *root->left;
        }
        else if(root->right != NULL && root->left != NULL){
            Node* temp = root->right;
            while(temp->left)temp = temp->left;
            root->data = temp->data;
            if(temp->right)*temp = *temp->right;
            else{
                temp = root;
                if(temp->right->data == root->data){
                    temp->right = NULL;
                }
                else{
                    temp = temp->right;
                    while(temp->left->data != root->data) temp = temp->left;
                    temp->left = NULL;
                }
            }
            return root;
        }
    }
}

string findData(Node* root, double data){
    if(data == root->data)return "TAK";
    if(data > root->data && root->right != NULL) return findData(root->right, data);
    if(data < root->data && root->left != NULL) return findData(root->left, data);
    return "NIE";
}

int countWhole(Node* root, int whole, int &counter){

    if(whole ==(int) root->data) counter++;
    if(root->right != NULL) countWhole(root->right, whole, counter);
    if(root->left != NULL) countWhole(root->left, whole, counter);
    return counter;
}

void show(Node* root,int space){
    for(int i=0;i<space;i++){
        cout<<"     ";
    }
    space++;
    cout<<root->data<<endl;
    if(root->right != NULL) show(root->right, space);
    if(root->left != NULL) show(root->left, space);
}

int main(){
    ifstream wejscie;
    double n, data, tempWhole, tempFraction;
    int counter = 0;
    char x;
    Node *root = NULL;
    wejscie.open("in1.txt");
    wejscie>>n;

    for(int i=0;i<n;i++){
        wejscie>>x>>tempWhole;
        if(wejscie.get() == ',') wejscie>>tempFraction;
        else tempFraction = 0;

        while(tempFraction>=1)tempFraction /= 10;

        data = tempWhole + tempFraction;

        switch(x){
            case 'W':
                root = add(root, data);
                break;
            case 'U':
                root = rem(root, data);
                break;
            case 'S':
                cout<<findData(root, data)<<endl;
                break;
            case 'L':
                cout<<countWhole(root, tempWhole, counter)<<endl;
                counter = 0;
                break;
        }
    }
    cout<<endl<<"drzewo:"<<endl;
    show(root,0);
}
